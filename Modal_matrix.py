import numpy as np
import xarray as xr
import dask

from xgcm import Grid

from Routines import utilities
import shutil














def Tmn_integral(Phi, e1u, e2v, e3t, ch_h, ch_v, ch_h_store, ch_v_store, datapath_data_preprocessed, datapath_temp, xgrid, max_mem):
    # Compute the Tmn matrix in integral form (cf These notes) and store them in the designated files.
    
    # input:
    # Phi: vertical pressure mode basis, on the T grid.
    # e1u : x axis metric on the u grid.
    # e2v : y axis metric on the v grid.
    # e3t : z axis metric on the t grid.
    # ch_h : horizontal chunking
    # ch_v : vertical chunking
    # ch_h_store : horizontal chunking used for storage, to decrease reading/writting operations.
    # ch_v_store : vertical chunking used for storage, to decrease reading/writting operations.
    # datapath_data_preprocessed: datapath of the directory where you are going to store Tmn_x and Tmn_y 
    # datapath_temp: datapath where to store temporary results.
    # plus all the temporary results.
    # xgrid xgcm object needed for interpolations and gradients.
    # max_mem : str, Max memory used per core by rechunker, no more than 3GB.
    
    # output:
    # nothing, well, Tmn_x and Tmn_y are stored in their respective file. and the routine doesn't return anything to you.
    
    
    # Initial rechunk of Phi with mode = -1.
    utilities.rechunk_warper(Phi.to_dataset(name='Phi'), {'z_c': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store, 'mode' : -1}, max_mem, datapath_temp/'Phi_prov_initial.zarr', datapath_temp/'Phi_temp.zarr')
    Phi = xr.open_zarr(datapath_temp/"Phi_prov_initial.zarr")
    Phi = Phi.Phi.chunk({'z_c': ch_v, 'x_c': ch_h, 'y_c': ch_h})
  

    # Computing the horizontal gradients of Phi
    utilities.diff_interp_storage(Phi, 'x', ch_h, ch_h_store, datapath_temp/"Phi_x_prov.zarr", datapath_temp/"Phi_x.zarr", 'PhiX', e1u, xgrid)
    utilities.diff_interp_storage(Phi, 'y', ch_h, ch_h_store, datapath_temp/"Phi_y_prov.zarr", datapath_temp/"Phi_y.zarr", 'PhiY', e2v, xgrid)
    
    
    
    # Rechunk Phi on the mode dimension to avoid getting too big chunk with a mode * modm matrix.
    # Need to reopen the Phi base rechunked because otherwise it put x_c chunk to -1 somewhere before and this makes 
    # the worker RAM to explode.
    Phi = xr.open_zarr(datapath_temp/"Phi_prov_initial.zarr")
    Phi = Phi.Phi
    utilities.rechunk_warper(Phi.to_dataset(name='Phi'), {'z_c': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store, 'mode' : 1}, max_mem, datapath_temp/'Phi_prov.zarr', datapath_temp/'Phi_temp.zarr')
    shutil.rmtree(datapath_temp/'Phi_prov_initial.zarr')
    Phi = xr.open_zarr(datapath_temp/"Phi_prov.zarr")
    Phi = Phi.Phi.chunk({'z_c': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    

    
    
    
    
    Phi_x = xr.open_zarr(datapath_temp/"Phi_x.zarr")
    Phi_x = Phi_x.PhiX.chunk({'z_c': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    Phi_y = xr.open_zarr(datapath_temp/"Phi_y.zarr")
    Phi_y = Phi_y.PhiY.chunk({'z_c': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    
    
    
    # Computing of Tmn_x and Tmn_y.
    step = 200
    
    Tmn_x = (Phi * Phi_x.rename({"mode":"modm"}) * e3t).sum('z_c') 
    utilities.storage_by_step(Tmn_x, 'TmnX', 'Tmn_x.zarr', step, {"x_c":ch_h_store, "y_c":ch_h_store}, datapath_data_preprocessed)
        
    Tmn_y = (Phi * Phi_y.rename({"mode":"modm"}) * e3t).sum('z_c') 
    utilities.storage_by_step(Tmn_y, 'TmnY', 'Tmn_y.zarr', step, {"x_c":ch_h_store, "y_c":ch_h_store}, datapath_data_preprocessed)
    

    
    # Final delete of temporary files
    shutil.rmtree(datapath_temp/'Phi_x.zarr')
    shutil.rmtree(datapath_temp/'Phi_y.zarr')
    shutil.rmtree(datapath_temp/'Phi_prov.zarr')
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
def Advection_uv_P1_matrix(Phi, ch_h, ch_v, ch_h_store, ch_v_store, e3t, data_preprocessed_path, datapath_temp, max_mem):
    # Compute the modal matrix used in the advection of u and v term P1 (P2 is the one depending on the gradient of vertical modes and is really small).

    # input:
    # Phi: vertical pressure modes, on the T grid
    # ch_h : horizontal chunking
    # ch_v : vertical chunking
    # ch_h_store : horizontal chunking used for storage, to decrease reading/writting operations.
    # ch_v_store : vertical chunking used for storage, to decrease reading/writting operations.
    # e3t: z metric z_c coordinates, on the T grid
    # data_preprocessed_path: datapath of the directory where you are going to store Anm
    # datapath_temp: datapath where to store temporary results.
    # max_mem : str, Max memory used per core by rechunker, no more than 3GB.

    # output:
    # Anm modal matrix used to compute the first part of the advection of u and v term.
    
    
    # Initial rechunk of Phi with mode = -1.
    utilities.rechunk_warper(Phi.to_dataset(name='Phi'), {'z_c': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store, 'mode' : -1}, max_mem, datapath_temp/'Phi_prov_initial.zarr', datapath_temp/'Phi_temp.zarr')
    Phi = xr.open_zarr(datapath_temp/"Phi_prov_initial.zarr")
    
    # chunking -1 on modm, 1 on mode and 1 on modk
    Phin = Phi.Phi.rename({"mode":"modm"})
    Phik = Phi.Phi.rename({"mode":"modk"})
    Phin = Phin.chunk({'z_c': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    Phik = Phik.chunk({'z_c': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    
    
    utilities.rechunk_warper(Phi, {'z_c': ch_v, 'x_c': ch_h, 'y_c': ch_h, 'mode' : 1}, max_mem, datapath_temp/'Phi_prov_initial_secundus.zarr', datapath_temp/'Phi_temp.zarr')
    Phi = xr.open_zarr(datapath_temp/"Phi_prov_initial_secundus.zarr")
    Phi = Phi.Phi.chunk({'z_c': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    


    


    Anm = (Phi * Phin * Phik * e3t).sum("z_c")

    return Anm





















def pressure_advection_matrix(Ns, Phiw, Phi, dc, e3w, ch_h, ch_v, ch_h_store, ch_v_store, xgrid,data_preprocessed_path, datapath_temp, max_mem):
    # Compute the modal matrix used in the advection of p term.

    # input:
    # Ns: mean stratification profiles, W grid
    # Phi: vertical pressure modes on the T grid
    # Phiw : vertical velocity modes on the W grid.
    # dc: modal eigenvalue (eigenspeed), T grid
    # e3w: z metric z_l coordinates
    # ch_h : horizontal chunking
    # ch_v : vertical chunking
    # ch_h_store : horizontal chunking used for storage, to decrease reading/writting operations.
    # ch_v_store : vertical chunking used for storage, to decrease reading/writting operations.
    # xgrid: xgcm grid object
    # data_preprocessed_path: datapath of the directory where you are going to store Bnm
    # datapath_temp: datapath where to store temporary results.
    # max_mem : str, Max memory used per core by rechunker, no more than 3GB.

    # output:
    # Bnm modal matrix used to compute the advection of pressure term.
    
    utilities.rechunk_warper(Phi.to_dataset(name='Phi'), {'z_c': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store, 'mode' : -1}, max_mem, datapath_temp/'Phi_prov_initial.zarr', datapath_temp/'Phi_temp.zarr')
    Phi = xr.open_zarr(datapath_temp/"Phi_prov_initial.zarr")
    Phi = Phi.Phi.chunk({'z_c': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    
    Phi = xgrid.interp(Phi, "Z", boundary="extend").chunk({"z_l":ch_v})
    Phi = Phi.chunk({'z_l': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store})
    Phi.to_dataset(name='Phi').to_zarr(datapath_temp/"Phi_prov.zarr", compute=True, mode='w')
    Phi = xr.open_zarr(datapath_temp/"Phi_prov.zarr")
    Phi = Phi.Phi.chunk({'z_l': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    shutil.rmtree(datapath_temp/"Phi_prov_initial.zarr")
    Phik = Phi.rename({"mode":"modk"})
    
    utilities.rechunk_warper(Phiw.to_dataset(name='Phiw'), {'z_l': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store, 'mode' : -1}, max_mem, datapath_temp/'Phiw_prov.zarr', datapath_temp/'Phiw_temp.zarr')
    Phiw = xr.open_zarr(datapath_temp/"Phiw_prov.zarr")
    Phiw = Phiw.Phiw.chunk({'z_l': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    Phiwm = Phiw.rename({"mode":"modm"})
    Phiw = Phiw.chunk({"mode":1})
    
    Ns = Ns.chunk({'z_l': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    
    dcm = dc.rename({"mode":"modm"}).chunk({'modm':-1})
    del dcm.encoding['chunks']
    dcm = dcm.chunk({'x_c': ch_h_store, 'y_c': ch_h_store})
    dcm.to_dataset(name='dc').to_zarr(datapath_temp/"dcm.zarr", compute=True, mode='w')
    dcm = xr.open_zarr(datapath_temp/"dcm.zarr")
    dcm = dcm.dc.chunk({'x_c': ch_h, 'y_c': ch_h})
    


    Bnm = (Ns/dcm**2 * Phik * Phiwm * Phiw * e3w).sum("z_l")



    return Bnm


    
    
    
    
    
    
    



    
    
    
def pressure_advection_matrix_without_N_dc(Phiw, Phi, e3w, ch_h, ch_v, ch_h_store, ch_v_store, xgrid,data_preprocessed_path, datapath_temp, max_mem):
    # Compute the modal matrix used in the advection of p term, but using an alternative formuation with vertical derivative of mdoes instead of Ns.
    # Allowing you to compute the matrix once and for all and apply corrections to transfert it to monthly bases afterward, wich is less costly that
    # recomputing it at each month because Ns changes.

    # input:
    # Ns: mean stratification profiles, W grid
    # Phi: vertical pressure modes on the T grid
    # Phiw : vertical velocity modes on the W grid.
    # dc: modal eigenvalue (eigenspeed), T grid
    # e3w: z metric z_l coordinates
    # ch_h : horizontal chunking
    # ch_v : vertical chunking
    # ch_h_store : horizontal chunking used for storage, to decrease reading/writting operations.
    # ch_v_store : vertical chunking used for storage, to decrease reading/writting operations.
    # xgrid: xgcm grid object
    # data_preprocessed_path: datapath of the directory where you are going to store Bnm
    # datapath_temp: datapath where to store temporary results.
    # max_mem : str, Max memory used per core by rechunker, no more than 3GB.

    # output:
    # Bnm modal matrix used to compute the advection of pressure term.
    
    utilities.rechunk_warper(Phi.to_dataset(name='Phi'), {'z_c': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store, 'mode' : -1}, max_mem, datapath_temp/'Phi_prov_initial.zarr', datapath_temp/'Phi_temp.zarr')
    
    Phi = xr.open_zarr(datapath_temp/"Phi_prov_initial.zarr")
    Phi = Phi.Phi.chunk({'z_c': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    
    Phik = Phi.rename({"mode":"modk"})
    Phik = xgrid.interp(Phik, "Z", boundary="extend").chunk({"z_l":ch_v})
    Phik = Phik.chunk({'z_l': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store})
    Phik.to_dataset(name='Phi').to_zarr(datapath_temp/"Phi_prov.zarr", compute=True, mode='w')
    Phik = xr.open_zarr(datapath_temp/"Phi_prov.zarr")
    Phik = Phik.Phi.chunk({'z_l': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    
    
    
    Phim = xgrid.diff(Phi, "Z", boundary="extend").chunk({"z_l":ch_v})
    Phim = Phim/e3w
    Phim = Phim.chunk({'z_l': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store})
    Phim.to_dataset(name='Phi').to_zarr(datapath_temp/"Phim_prov.zarr", compute=True, mode='w')
    Phim = xr.open_zarr(datapath_temp/"Phim_prov.zarr")
    Phim = Phim.Phi.chunk({'z_l': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    Phim = Phim.rename({"mode":"modm"})
    shutil.rmtree(datapath_temp/"Phi_prov_initial.zarr")
    
    
    
    
    utilities.rechunk_warper(Phiw.to_dataset(name='Phiw'), {'z_l': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store, 'mode' : -1}, max_mem, datapath_temp/'Phiw_prov.zarr', datapath_temp/'Phiw_temp.zarr')
    Phiw = xr.open_zarr(datapath_temp/"Phiw_prov.zarr")
    Phiw = Phiw.Phiw.chunk({'z_l': ch_v, 'x_c': ch_h, 'y_c': ch_h})

    
    Bnm = (Phik * Phim * Phiw * e3w).sum("z_l")



    return Bnm

















def vertical_shear_matrix(Phi, Phiw, e3w, xgrid, ch_h, ch_v, ch_h_store, ch_v_store, datapath_temp, max_mem):
    # Compute the modal matrix used in the background velocity vertical shear term.

    # input:
    # Phi: vertical pressure modes on the T grid
    # Phiw : vertical velocity modes on the W grid.
    # e3w: z metric z_l coordinates
    # ch_h : horizontal chunking
    # ch_v : vertical chunking
    # ch_h_store : horizontal chunking used for storage, to decrease reading/writting operations.
    # ch_v_store : vertical chunking used for storage, to decrease reading/writting operations.
    # xgrid: xgcm grid object
    # datapath_temp: datapath where to store temporary results.
    # max_mem : str, Max memory used per core by rechunker, no more than 3GB.

    # output:
    # Anm_z modal matrix used to compute the background velocity vertical shear term.

    
    
    utilities.rechunk_warper(Phi.to_dataset(name='Phi'), {'z_c': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store, 'mode' : -1}, max_mem, datapath_temp/'Phi_prov_initial.zarr', datapath_temp/'Phi_temp.zarr')
    Phi = xr.open_zarr(datapath_temp/"Phi_prov_initial.zarr")
    Phi = Phi.Phi.chunk({'z_c': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    Phi_z = xgrid.diff(Phi, 'Z', boundary="extend")/e3w
    Phi_z = Phi_z.chunk({'z_l': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store})
    Phi_z.to_dataset(name='Phi').to_zarr(datapath_temp/"Phi_z_prov.zarr", compute=True, mode='w')
    Phi_z = xr.open_zarr(datapath_temp/"Phi_z_prov.zarr")
    Phi_z = Phi_z.Phi.chunk({'z_l': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    Phik_z = Phi_z.rename({"mode":"modk"})
    

    Phi = xgrid.interp(Phi, "Z", boundary="extend").chunk({"z_l":ch_v})
    Phi = Phi.chunk({'z_l': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store})
    Phi.to_dataset(name='Phi').to_zarr(datapath_temp/"Phi_prov.zarr", compute=True, mode='w')
    Phi = xr.open_zarr(datapath_temp/"Phi_prov.zarr")
    Phi = Phi.Phi.chunk({'z_l': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    shutil.rmtree(datapath_temp/"Phi_prov_initial.zarr")
    
    
    utilities.rechunk_warper(Phiw.to_dataset(name='Phiw'), {'z_l': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store, 'mode' : -1}, max_mem, datapath_temp/'Phiw_prov.zarr', datapath_temp/'Phiw_temp.zarr')
    Phiwm = xr.open_zarr(datapath_temp/"Phiw_prov.zarr")
    Phiwm = Phiwm.Phiw.rename({"mode":"modm"})
    Phiwm = Phiwm.chunk({'z_l': ch_v, 'x_c': ch_h, 'y_c': ch_h})

    
    

    
    
    Anm_z = -(Phik_z * Phiwm * Phi * e3w).sum('z_l') # minus because of the way the z axis is oriented
    
    return Anm_z
















def horizontal_shear_matrix_X(Phi, e3t, e1u, xgrid, ch_h, ch_v, ch_h_store, ch_v_store, datapath_temp, max_mem):
    
    # Compute one modal matrix used in the background velocity horizontal shear term (The one containing the X gradient of pressure mode), can also be used in the second part of the advection of uv internal tide term.

    # input:
    # Phi : vertical pressure modes on the T grid.
    # e1u : horizontal metric for x axis of the U grid.
    # e3t : vertical metric for z axis of the T grid.
    # ch_h : horizontal chunking.
    # ch_v : vertical chunking.
    # ch_h_store : horizontal chunking used for storage, to decrease reading/writting operations.
    # ch_v_store : vertical chunking used for storage, to decrease reading/writting operations.
    # xgrid: xgcm grid object.
    # datapath_temp: datapath where to store temporary results.
    # max_mem : str, Max memory used per core by rechunker, no more than 3GB.

    # output:
    # Anm_x modal matrix used to compute the background velocity vertical shear term.

    
    
    utilities.rechunk_warper(Phi.to_dataset(name='Phi'), {'z_c': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store, 'mode' : -1}, max_mem, datapath_temp/'Phi_prov_initial.zarr', datapath_temp/'Phi_temp.zarr')
    Phi = xr.open_zarr(datapath_temp/"Phi_prov_initial.zarr")
    Phi = Phi.Phi.chunk({'z_c': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    Phim = Phi.rename({"mode":"modm"})
    Phim = Phim.chunk({'z_c': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    
    Phik = Phi.rename({"mode":"modk"})
    Phik_x = xgrid.diff(Phik, "X", boundary="extend").chunk({"x_r":ch_h})/e1u
    Phik_x = Phik_x.chunk({'z_c': ch_v_store, 'x_r': ch_h_store, 'y_c': ch_h_store, 'modk':-1})
    Phik_x.to_dataset(name='Phi').to_zarr(datapath_temp/"Phi_prov_x.zarr", compute=True, mode='w')
    Phik_x = xr.open_zarr(datapath_temp/"Phi_prov_x.zarr")
    Phik_x = Phik_x.Phi.chunk({'z_c': ch_v, 'x_r': ch_h, 'y_c': ch_h})
    Phik_x = xgrid.interp(Phik_x, "X", boundary="extend").chunk({"x_c":ch_h})
    Phik_x = Phik_x.chunk({'z_c': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store})
    Phik_x.to_dataset(name='Phi').to_zarr(datapath_temp/"Phi_prov_x_secundus.zarr", compute=True, mode='w')
    Phik_x = xr.open_zarr(datapath_temp/"Phi_prov_x_secundus.zarr")
    Phik_x = Phik_x.Phi.chunk({'z_c': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    shutil.rmtree(datapath_temp/"Phi_prov_x.zarr")
    
    
    



    

    

    
    
    Anm_x = (Phik_x * Phim * Phi * e3t).sum('z_c')
    
    return Anm_x


















def horizontal_shear_matrix_Y(Phi, e3t, e2v, xgrid, ch_h, ch_v, ch_h_store, ch_v_store, datapath_temp, max_mem):
    # Compute one modal matrix used in the background velocity horizontal shear term (The one containing the Y gradient of pressure mode).

    # input:
    # Phi : vertical pressure modes on the T grid.
    # e2v : horizontal metric for y axis of the V grid.
    # e3t : vertical metric for z axis of the T grid.
    # ch_h : horizontal chunking.
    # ch_v : vertical chunking.
    # ch_h_store : horizontal chunking used for storage, to decrease reading/writting operations.
    # ch_v_store : vertical chunking used for storage, to decrease reading/writting operations.
    # xgrid: xgcm grid object.
    # datapath_temp: datapath where to store temporary results.
    # max_mem : str, Max memory used per core by rechunker, no more than 3GB.

    # output:
    # Anm_y modal matrix used to compute the background velocity vertical shear term.

    
    
    utilities.rechunk_warper(Phi.to_dataset(name='Phi'), {'z_c': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store, 'mode' : -1}, max_mem, datapath_temp/'Phi_prov_initial.zarr', datapath_temp/'Phi_temp.zarr')
    Phi = xr.open_zarr(datapath_temp/"Phi_prov_initial.zarr")
    Phi = Phi.Phi.chunk({'z_c': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    Phim = Phi.rename({"mode":"modm"})
    Phim = Phim.chunk({'z_c': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    
    Phik = Phi.rename({"mode":"modk"})
    Phik_y = xgrid.diff(Phik, "Y", boundary="extend").chunk({"y_r":ch_h})/e2v
    Phik_y = Phik_y.chunk({'z_c': ch_v_store, 'x_c': ch_h_store, 'y_r': ch_h_store, 'modk':-1})
    Phik_y.to_dataset(name='Phi').to_zarr(datapath_temp/"Phi_prov_y.zarr", compute=True, mode='w')
    Phik_y = xr.open_zarr(datapath_temp/"Phi_prov_y.zarr")
    Phik_y = Phik_y.Phi.chunk({'z_c': ch_v, 'x_c': ch_h, 'y_r': ch_h})
    Phik_y = xgrid.interp(Phik_y, "Y", boundary="extend").chunk({"y_c":ch_h})
    Phik_y = Phik_y.chunk({'z_c': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store})
    Phik_y.to_dataset(name='Phi').to_zarr(datapath_temp/"Phi_prov_y_secundus.zarr", compute=True, mode='w')
    Phik_y = xr.open_zarr(datapath_temp/"Phi_prov_y_secundus.zarr")
    Phik_y = Phik_y.Phi.chunk({'z_c': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    shutil.rmtree(datapath_temp/"Phi_prov_y.zarr")
    
    
    
    
 

    

    
    
    Anm_y = (Phik_y * Phim * Phi * e3t).sum('z_c')
    
    return Anm_y
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
def Advection_Ns_matrix(Phi, Phiw, N, c, e1u, e2v, e3t, e3w, ch_h, ch_v, ch_h_store, ch_v_store, datapath_temp, max_mem, xgrid):
    # Compute the three modal matrix used in the advection of steady stratification by the background flow coupling term.
    
    # input :
    # Phi : datarray, vertical pressure modes, T grid.
    # Phiw : datarray, vertical velocity modes, W grid.
    # N : datarray, mean stratification profile of the period considered.
    # c : datarray, vertical modes eigenvalue, T grid.
    # e1u : datarray, metric on the X axis, U grid.
    # e2v : datarray, metric on the Y axis, V grid.
    # e3t : datarray, metric on the Z axis, T grid.
    # e3w : datarray, metric on the Z axis, W grid.
    # ch_h : interger, horizontal chunking used for computation.
    # ch_v : interger, vertical chunking used for computation.
    # ch_h_store : interger, horizontal chunking used for storage.
    # ch_v_store : interger, vertical chunking used for storage.
    # datapath_temp : path where to stock preliminary results.
    # max_mem : string, max memory allowed per thread to rechunker.
    # xgrid : xgrid object.
    
    # output:
    # Bnm_x, Bnm_y, Bnm_z, matrix used in the advection of steady stratification by the background flow coupling term.
    
    
    

    
    
    utilities.rechunk_warper(Phi.to_dataset(name='Phi'), {'z_c': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store, 'mode' : -1}, max_mem, datapath_temp/'Phi_prov_initial.zarr', datapath_temp/'Phi_temp.zarr')
    Phi = xr.open_zarr(datapath_temp/"Phi_prov_initial.zarr")
    Phi = Phi.Phi.chunk({'z_c': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    Phi = xgrid.interp(Phi, "Z", boundary="extend").chunk({"z_l":ch_v})
    Phi = Phi.chunk({'z_l': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store})
    Phi.to_dataset(name='Phi').to_zarr(datapath_temp/"Phi_prov_secundus.zarr", compute=True, mode='w')
    Phi = xr.open_zarr(datapath_temp/"Phi_prov_secundus.zarr")
    Phi = Phi.Phi.chunk({'z_l': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    Phik = Phi.rename({"mode":"modk"})
    shutil.rmtree(datapath_temp/"Phi_prov_initial.zarr")
    
    utilities.rechunk_warper(Phiw.to_dataset(name='Phiw'), {'z_l': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store, 'mode' : -1}, max_mem, datapath_temp/'Phiw_prov.zarr', datapath_temp/'Phiw_temp.zarr')
    Phiw = xr.open_zarr(datapath_temp/"Phiw_prov.zarr")
    Phiw = Phiw.Phiw.chunk({'z_l': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    Phiwm = Phiw.rename({"mode":"modm"})
    Phiw = Phiw.chunk({'z_l': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store})
    utilities.rechunk_warper(Phiw.to_dataset(name='Phiw'), {'z_l': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store, 'mode' : 1}, max_mem, datapath_temp/'Phiw_prov_secundus.zarr', datapath_temp/'Phiw_temp.zarr')
    Phiw = xr.open_zarr(datapath_temp/"Phiw_prov_secundus.zarr")
    Phiw = Phiw.Phiw.chunk({'z_l': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    
    
    
    
    cm = c.rename({"mode":"modm"})
    
    utilities.diff_interp_storage(N * Phiwm / cm**2, 'x', ch_h, ch_h_store, datapath_temp/"Ng_x_prov.zarr", datapath_temp/"Ng_x.zarr", 'Ng', e1u, xgrid)
    utilities.diff_interp_storage(N * Phiwm / cm**2, 'y', ch_h, ch_h_store, datapath_temp/"Ng_y_prov.zarr", datapath_temp/"Ng_y.zarr", 'Ng', e2v, xgrid)
    utilities.diff_interp_storage(N * Phiwm / cm**2, 'z', ch_v, ch_v_store, datapath_temp/"Ng_z_prov.zarr", datapath_temp/"Ng_z.zarr", 'Ng', e3t, xgrid)
    
    
    Ng_x = xr.open_zarr(datapath_temp/"Ng_x.zarr")
    Ng_x = Ng_x.Ng.chunk({'z_l': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    
    Ng_y = xr.open_zarr(datapath_temp/"Ng_y.zarr")
    Ng_y = Ng_y.Ng.chunk({'z_l': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    
    Ng_z = xr.open_zarr(datapath_temp/"Ng_z.zarr")
    Ng_z = Ng_z.Ng.chunk({'z_l': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    Ng_z = -Ng_z  # because of the z axis orientation
        
        
    Bnm_x = (Ng_x * Phiw * Phik * e3w).sum("z_l")
    Bnm_y = (Ng_y * Phiw * Phik * e3w).sum("z_l")
    Bnm_z = (Ng_z * Phiw * Phik * e3w).sum("z_l")
    
    return Bnm_x, Bnm_y, Bnm_z


















def Advection_Ns_matrix_without_Ns(Phi, Phiw, e1u, e2v, e3t, e3w, ch_h, ch_v, ch_h_store, ch_v_store, datapath_temp, max_mem, xgrid):
    # Compute the three modal matrix used in the advection of steady stratification by the background flow coupling term, but using an alternative formuation with vertical derivative of mdoes instead of Ns.
    # Allowing you to compute the matrix once and for all and apply corrections to transfert it to monthly bases afterward, wich is less costly that
    # recomputing it at each month because Ns changes.
    
    # input :
    # Phi : datarray, vertical pressure modes, T grid.
    # Phiw : datarray, vertical velocity modes, W grid.
    # e1u : datarray, metric on the X axis, U grid.
    # e2v : datarray, metric on the Y axis, V grid.
    # e3t : datarray, metric on the Z axis, T grid.
    # e3w : datarray, metric on the Z axis, W grid.
    # ch_h : interger, horizontal chunking used for computation.
    # ch_v : interger, vertical chunking used for computation.
    # ch_h_store : interger, horizontal chunking used for storage.
    # ch_v_store : interger, vertical chunking used for storage.
    # datapath_temp : path where to stock preliminary results.
    # max_mem : string, max memory allowed per thread to rechunker.
    # xgrid : xgrid object.
    
    # output:
    # Bnm_x, Bnm_y, Bnm_z, matrix used in the advection of steady stratification by the background flow coupling term.
    
    
    

    
    
    utilities.rechunk_warper(Phi.to_dataset(name='Phi'), {'z_c': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store, 'mode' : -1}, max_mem, datapath_temp/'Phi_prov_initial.zarr', datapath_temp/'Phi_temp.zarr')
    Phi = xr.open_zarr(datapath_temp/"Phi_prov_initial.zarr")
    Phi = Phi.Phi.chunk({'z_c': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    Phi_z = xgrid.diff(Phi, 'Z', boundary="extend").chunk({"z_l":ch_h})/e3w
    Phi_z = -Phi_z #because of the z axis orientation
    Phi_z = Phi_z.chunk({'z_l': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store})
    Phi_z.to_dataset(name='Phi').to_zarr(datapath_temp/"Phi_z_prov.zarr", compute=True, mode='w')
    
    
    
    Phi = xgrid.interp(Phi, "Z", boundary="extend").chunk({"z_l":ch_v})
    Phi = Phi.chunk({'z_l': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store})
    Phi.to_dataset(name='Phi').to_zarr(datapath_temp/"Phi_prov_secundus.zarr", compute=True, mode='w')
    Phi = xr.open_zarr(datapath_temp/"Phi_prov_secundus.zarr")
    Phi = Phi.Phi.chunk({'z_l': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    Phik = Phi.rename({"mode":"modk"})
    shutil.rmtree(datapath_temp/"Phi_prov_initial.zarr")
    
    utilities.rechunk_warper(Phiw.to_dataset(name='Phiw'), {'z_l': ch_v_store, 'x_c': ch_h_store, 'y_c': ch_h_store, 'mode' : -1}, max_mem, datapath_temp/'Phiw_prov.zarr', datapath_temp/'Phiw_temp.zarr')
    Phiw = xr.open_zarr(datapath_temp/"Phiw_prov.zarr")
    Phiw = Phiw.Phiw.chunk({'z_l': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    
    Phi_z = xr.open_zarr(datapath_temp/"Phi_z_prov.zarr")
    Phi_zm = Phi_z.Phi 
    
    
    
    
    
    utilities.diff_interp_storage(Phi_zm, 'x', ch_h, ch_h_store, datapath_temp/"Ng_x_prov.zarr", datapath_temp/"Ng_x.zarr", 'Ng', e1u, xgrid)
    utilities.diff_interp_storage(Phi_zm, 'y', ch_h, ch_h_store, datapath_temp/"Ng_y_prov.zarr", datapath_temp/"Ng_y.zarr", 'Ng', e2v, xgrid)
    
    # The diff_interp_storage routine is not suited to process vertical modes (fields with z diemnsion.)
    Phi_zm = xgrid.diff(Phi_zm, 'Z', boundary="extend").chunk({"z_c":ch_v})/e3t
    Phi_zm = Phi_zm.chunk({'x_c':ch_h_store, 'y_c':ch_h_store, 'z_c':ch_v_store})
    Phi_zm.to_dataset(name='Phi').to_zarr(datapath_temp/'Ng_z_prov.zarr', compute=True, mode='w')
    Phi_zm = xr.open_zarr(datapath_temp/'Ng_z_prov.zarr')
    Phi_zm = Phi_zm.Phi.chunk({'x_c':ch_h, 'y_c':ch_h, 'z_c':ch_v})
    Phi_zm = xgrid.interp(Phi_zm, 'Z', boundary="extend").chunk({"z_l":ch_v})
    Phi_zm = Phi_zm.chunk({'x_c':ch_h_store, 'y_c':ch_h_store, 'z_l':ch_v_store})
    Phi_zm.to_dataset(name='Ng').to_zarr(datapath_temp/'Ng_z.zarr', compute=True, mode='w')
    shutil.rmtree(datapath_temp/"Ng_z_prov.zarr")

    
    
    Ng_x = xr.open_zarr(datapath_temp/"Ng_x.zarr")
    Ng_x = Ng_x.Ng.chunk({'z_l': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    
    Ng_y = xr.open_zarr(datapath_temp/"Ng_y.zarr")
    Ng_y = Ng_y.Ng.chunk({'z_l': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    
    Ng_z = xr.open_zarr(datapath_temp/"Ng_z.zarr")
    Ng_z = Ng_z.Ng.chunk({'z_l': ch_v, 'x_c': ch_h, 'y_c': ch_h})
    Ng_z = -Ng_z  #because of the z axis orientation
    
    # The renaming of mode to modm is needed to be done after all operation involving xgrid (interp, diff), since xgrid doesn't know the modm dimension
    # because of using Aus that doesn't have it either for building it, leading xgrid to drop the dim modm if it is passed an object with it.
    Ng_x = Ng_x.rename({"mode":"modm"})  
    Ng_y = Ng_y.rename({"mode":"modm"}) 
    Ng_z = Ng_z.rename({"mode":"modm"}) 
        
        
    Bnm_x = (-Ng_x * Phiw * Phik * e3w).sum("z_l")
    Bnm_y = (-Ng_y * Phiw * Phik * e3w).sum("z_l")
    Bnm_z = (-Ng_z * Phiw * Phik * e3w).sum("z_l")
    
    return Bnm_x, Bnm_y, Bnm_z










def Tmn_perturb_H_x(Phi, H, e1u, tmask, dc, ch_h, datapath_data_preprocessed, xgrid):
    # Compute Tmn_x using the perturbation method, topographic component.
    
    # input:
    # Phi : datarray, vertical pressure modes, T grid.
    # H : datarray, topography, T grid.
    # e1u : datarray, metrix of x axis, U grid.
    # tmask : datarray, mask of the T grid 3D.
    # dc : datarray, eigenvalues.
    # ch_h : integer, horizontal chunking.
    # datapath_data_preprocessed : Path, datapatj where to stock the result.
    
    
    
    Hx = xgrid.diff(H, 'X', boundary="extend").chunk({"x_r":ch_h})/e1u
    Hx = xgrid.interp(Hx, 'X', boundary="extend").chunk({"x_c":ch_h})
    
    step = 200
    LENGTH = len(Phi.coords['y_c'].values)
    
    for i in range(0, LENGTH, step):
        a = min(step, LENGTH - i)
        Selection = range(i,i+a, 1)

        print(i)
        
        HxS = Hx.isel(y_c=Selection)
        tmaskS = tmask.isel(y_c=Selection)
        PhiS = Phi.isel(y_c=Selection)
        dcS = dc.isel(y_c=Selection)
        
        HxS = HxS.chunk({"x_c":ch_h, "y_c":ch_h})

        indz = tmaskS.sum("z_c")
        indz = indz.where(indz>0, tmask.z_c.size) # trick to get Nan on land

        indz = indz.chunk({"x_c":ch_h, "y_c":ch_h})
        phibottom = PhiS.sel(z_c = indz)
        phibottom = phibottom.reset_coords(drop=True)
        phibottom = phibottom.chunk({"x_c":ch_h, "y_c":ch_h, 'mode':-1})
    


        




        Tmn_x = dcS**2/(dcS.rename({"mode":"modm"})**2-dcS**2)*phibottom*phibottom.rename({"mode":"modm"})*HxS    
        


        #for n=m.
        Tmn_x = xr.where(Tmn_x.mode == Tmn_x.modm, 0.5*(1-phibottom**2)*Hx, Tmn_x) 
        Tmn_x = Tmn_x.reset_coords(drop=True)
        B = Tmn_x.coords
        Tmn_x = Tmn_x.drop_vars(B)
      
        Tmn_x = Tmn_x.chunk({'x_c':ch_h*2, 'y_c':ch_h*2, 'mode':-1, 'modm':-1})
        if i == 0:
            Tmn_x.to_dataset(name='Tmn_x').to_zarr(datapath_data_preprocessed/'Tmn_x_perturb_H.zarr', compute=True, mode='w') #store in the zarr store created above 
        else:
            Tmn_x.to_dataset(name='Tmn_x').to_zarr(datapath_data_preprocessed/'Tmn_x_perturb_H.zarr', compute=True, mode='a', append_dim='y_c') #store in the zarr store created above 

        
        
        
        
        
        
        








def Tmn_perturb_H_y(Phi, H, e2v, tmask, dc, ch_h, datapath_data_preprocessed, xgrid):
    # Compute Tmn_y using the perturbation method, topographic component.
    
    # input:
    # Phi : datarray, vertical pressure modes, T grid.
    # H : datarray, topography, T grid.
    # e2v : datarray, metrix of y axis, V grid.
    # tmask : datarray, mask of the T grid 3D.
    # dc : datarray, eigenvalues.
    # ch_h : integer, horizontal chunking.
    # datapath_data_preprocessed : Path, datapatj where to stock the result.
    
    
    Hy = xgrid.diff(H, 'Y', boundary="extend").chunk({"y_r":ch_h})/e2v
    Hy = xgrid.interp(Hy, 'Y', boundary="extend").chunk({"y_c":ch_h})
    
    step = 200
    LENGTH = len(Phi.coords['y_c'].values)
    
    for i in range(0, LENGTH, step):
        a = min(step, LENGTH - i)
        Selection = range(i,i+a, 1)

        print(i)
        
        HyS = Hy.isel(y_c=Selection)
        tmaskS = tmask.isel(y_c=Selection)
        PhiS = Phi.isel(y_c=Selection)
        dcS = dc.isel(y_c=Selection)
        
        HyS = HyS.chunk({"x_c":ch_h, "y_c":ch_h})

        indz = tmaskS.sum("z_c")
        indz = indz.where(indz>0, tmask.z_c.size) # trick to get Nan on land

        indz = indz.chunk({"x_c":ch_h, "y_c":ch_h})
        phibottom = PhiS.sel(z_c = indz)
        phibottom = phibottom.reset_coords(drop=True)
        phibottom = phibottom.chunk({"x_c":ch_h, "y_c":ch_h, 'mode':-1})
    


        




        Tmn_y = dcS**2/(dcS.rename({"mode":"modm"})**2-dcS**2)*phibottom*phibottom.rename({"mode":"modm"})*HyS
    
        


        #for n=m.
        Tmn_y = xr.where(Tmn_y.mode == Tmn_y.modm, 0.5*(1-phibottom**2)*Hy, Tmn_y)
        
        Tmn_y = Tmn_y.reset_coords(drop=True)
        B = Tmn_y.coords
        Tmn_y = Tmn_y.drop_vars(B)
      
        Tmn_y = Tmn_y.chunk({'x_c':ch_h*2, 'y_c':ch_h*2, 'mode':-1, 'modm':-1})
        if i == 0:
            Tmn_y.to_dataset(name='Tmn_y').to_zarr(datapath_data_preprocessed/'Tmn_y_perturb_H.zarr', compute=True, mode='w') #store in the zarr store created above 
        else:
            Tmn_y.to_dataset(name='Tmn_y').to_zarr(datapath_data_preprocessed/'Tmn_y_perturb_H.zarr', compute=True, mode='a', append_dim='y_c') #store in the zarr store created above 
   