import numpy as np
import xarray as xr
import dask

from xgcm import Grid

from Routines import utilities



def Modal_pressure_amplitude_correction(data_p, ssh, Pcor, Anm):
    # Apply the correction of the bad projection of the barotropic mode on the baroclinic modes, and then the corrections needed
    # because of the wrong Brunt Vaisala frequency used to compute the vertical modes.
    
    # corr = ssh * pcor
    # To add to the pressure modal amplitude after computation.

    # corr = (pn.rename(modm)*Anm).sum('modm')

    # input:
    # data_p : modal pressure anomaly, on the T grid
    # ssh: ssh
    # Pcor: Pcor
    # Correction matrix, on the T grid

    # output:
    # data_p : Modal presure anomaly corrected, T grid

    Pcor = ssh * Pcor

    data_p = data_p + Pcor

    data_p = (data_p.rename({"mode":"modm"}) * Anm).sum('modm')
    
    return data_p



def Modal_velocity_amplitude_correction(data, Anm):
    # Apply the correction for the wrong Brunt Vaisala frequency profile and the interpolation of the modal bases before projection to the 
    # zonal velocity modal amplitudes. Both correction are grouped in one matrix.

    # input:
    # data : modal velocity anomaly uncorrected, Mesoscale or Internal tides, on the U or V grid.
    # Anm : Correction matrix for the two corrections, on the same grid as data.

    # output:
    # data : Modal velocity amplitudes corrected, U or V grid.

    data = (data.rename({"mode":"modm"}) * Anm).sum('modm')

    return data