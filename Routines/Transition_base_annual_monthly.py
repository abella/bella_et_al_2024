import numpy as np
import xarray as xr
import dask

from xgcm import Grid

import scipy as sc
import scipy.signal as sig
from scipy.ndimage import gaussian_filter
from scipy.signal import freqz
from scipy.signal import butter, lfilter

from Routines import filtres
from Routines import utilities

from rechunker import rechunk

import shutil

import numpy.ma as ma





def transition_monthly_base_t(Pnm, data_t, ch_h):
    # Put a datarray on the t grid from the global eNATL60 base to a more local one.
    
    # input : 
    # Pnm : datarray, transition matrix from global to local.
    # data_t : datarray, field to correct, on the t grid.
    # ch_h : horizontal chunking used for computation.
    
    # output :
    # data_t : datarray on the local base.
    

    
    data_t = data_t.rename({'mode':'modm'})
    data_t = (Pnm*data_t).sum('modm')
    
    return data_t


    
    
def transition_monthly_base_u(Pnm, data_u, xgrid, ch_h):
    # Put a datarray on the u grid from the global eNATL60 base to a more local one.
    
    # input : 
    # Pnm : datarray, transition matrix from global to local.
    # data_u : datarray, field to correct, on the u grid.
    # xgrid : xgrid object.
    # ch_h : horizontal chunking used for computation.
    
    # output :
    # data_u : datarray on the local base.
    
    
    Pnm_u = xgrid.interp(Pnm, "X", boundary="extend").chunk({"x_r":ch_h})
    
    data_u = data_u.rename({'mode':'modm'})
    data_u = (Pnm_u*data_u).sum('modm')
    
    return data_u
    
    
def transition_monthly_base_v(Pnm, data_v, xgrid, ch_h):
    # Put a datarray on the v grid from the global eNATL60 base to a more local one.
    
    # input : 
    # Pnm : datarray, transition matrix from global to local.
    # data_v : datarray, field to correct, on the v grid.
    # xgrid : xgrid object.
    # ch_h : horizontal chunking used for computation.
    
    # output :
    # data_v : datarray on the local base.
    
    
    Pnm_v = xgrid.interp(Pnm, "Y", boundary="extend").chunk({"y_r":ch_h})
    
    data_v = data_v.rename({'mode':'modm'})
    data_v = (Pnm_v*data_v).sum('modm')
    
    return data_v







def Passage_annuel_mensuel_Tmn_x_y(Tmn, conponent, Pnm, Modal_matrix, horizontal_metric, ch_h, datapath_temp, xgrid, step):
    # Put the Tmn_x or Tmn_y matrix from the annual to the monthly base.
    
    # input:
    # Tmn : datarray, Tmn_x or Tmn_y modal matrix, on the T grid.
    # conponent : str, X or Y, to processe Tmn_x or Tmn_y.
    # Pnm : dataray, Transition matrix from the annual (simulation base) to the monthly base, on the T grid.
    # e3t : datarray, vertical metric, T grid.
    # Phim : datarray, vertical pressure mode, T grid.
    # horizontal metric : datarray, horizontal metric along the relevant axis, u or v grid.
    # ch_h : integer, chunking used in the horizontal directions during computation.
    
    ch_h_store = ch_h*2

    
    Tmn = (Tmn.rename({'mode':'modi'}) * Pnm.rename({'mode':'modi', 'modm':'mode'}).chunk({"mode":6})).sum('modi')
    Tmn = (Tmn.rename({'modm':'modi'}) * Pnm.rename({'mode':'modi'}).chunk({"modi":-1})).sum('modi')
    
    Modal_matrix = (Modal_matrix.rename({'mode':'modi'}) * Pnm.rename({'mode':'modi', 'modm':'mode'}).chunk({"mode":6})).sum('modi')
    
    if conponent == 'X':
        Pnm_diff = xgrid.diff(Pnm, conponent, boundary='extend').chunk({'x_r': ch_h})/horizontal_metric
        Pnm_diff = xgrid.interp(Pnm_diff, conponent, boundary="extend").chunk({"x_c":ch_h})
    if conponent == 'Y':
        Pnm_diff = xgrid.diff(Pnm, conponent, boundary='extend').chunk({'y_r': ch_h})/horizontal_metric
        Pnm_diff = xgrid.interp(Pnm_diff, conponent, boundary="extend").chunk({"y_c":ch_h})
        
    Modal_matrix = (Modal_matrix.rename({'mode':'modi'}) * Pnm_diff.rename({'mode':'modi'}).chunk({"modi":-1})).sum('modi')
         
    Tmn = Tmn + Modal_matrix
    
    
    if conponent == 'X':
        utilities.storage_by_step(Tmn, 'Tmn_x', "Tmn_x_month.zarr", step, {"x_c":ch_h_store, "y_c":ch_h_store}, datapath_temp)  
    if conponent == 'Y':
        utilities.storage_by_step(Tmn, 'Tmn_y', "Tmn_y_month.zarr", step, {"x_c":ch_h_store, "y_c":ch_h_store}, datapath_temp)  


        
        
        
        
        
        
def Transition_Anm_x_y(Pnm, Anm, Anm_grad, variable_name, File_name_temp, File_name_temp_secundus, File_name_final, step, ch_h, datapath_temp):
    # Apply the corrections needed to put the Anm_x or Anm_y matrix on an monthly base, from a global one.
    
    # input:
    # Pnm: datarray, transition matrix, T grid
    # Anm: datarray, modal matrix used in the advection of uv coupling term, T grid.
    # Anm_grad: datarray, Anm_x or Anm_y matrix used in the horizontal shear coupling term, T grid.
    # variable_name : string, name you want to give to the final matrix dataset.
    # File_name_temp : string, name of file for temporary storage.
    # File_name_temp_secundus : string, name of file for temporary storage.
    # File_name_final : string, name of the file of the final matrix.
    # step : integer, size of the step used on the y axis to break the computation into smaller part and make it manageable by the computer
    # ch_h : integer, chunking used for computation.
    
    
    #Anm_F = (Anm.rename({"modm":"modi"}) * Pnm.rename({"mode":"modi"}).chunk({"modi":-1})).sum('modi')
    #Anm_F = (Anm_F.rename({"mode":"modi"}) * Pnm.rename({"mode":"modi", "modm":"mode"}).chunk({"modi":-1, "mode":1})).sum('modi')
    
    #utilities.storage_by_step(Anm_F, variable_name, File_name_temp, step, {"x_c":ch_h*2, "y_c":ch_h*2, 'mode':-1, 'modk':-1}, datapath_temp)
                

 
        
        
        
    Anm_gradP = (Anm_grad.rename({"modm":"modi"}) * Pnm.rename({"mode":"modi"}).chunk({"modi":-1})).sum('modi')
    Anm_gradP = (Anm_gradP.rename({"mode":"modi"}) * Pnm.rename({"mode":"modi", "modm":"mode"}).chunk({"mode":1})).sum('modi')
    
    utilities.storage_by_step(Anm_gradP, variable_name, File_name_final, step, {"x_c":ch_h*2, "y_c":ch_h*2, 'mode':-1, 'modk':-1}, datapath_temp)

