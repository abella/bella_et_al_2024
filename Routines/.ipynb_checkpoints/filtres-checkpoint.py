import numpy as np
import xarray as xr
import dask

from xgcm import Grid

import scipy as sc
import scipy.signal as sig
from scipy.ndimage import gaussian_filter
from scipy.signal import freqz
from scipy.signal import butter, lfilter


def datetime_to_hour(t, t0):
    """return time interval in hours between t (a dataarray of datetime objects) and t0 (a datetime object)
warning: not sure it works if you swith month or year..."""
    dt = (t - t0).dt
    return dt.seconds/3600 + dt.days*24



def filtre_demodulation_complexe_passe_bas(data, omega,dt, fc,N,coord="t"):
    #data: data to process, depends on x, y t, t must be the first dim
    #omega: pulsation of the center of the frequency domain of interest, rad/h
    #dt: time step in hour
    #fc: cutting frequency of the lowpass filter
    #N: order of the filter
    
    #Return:
    #Res: reconstructed filtered signal of the input data
    #ComplexAmplitude: Complex amplitude of the complex demodulation of the input data
    
    A = data.shape
    dimt = A[0]
    dimspace1 = A[1]
    dimspace2 = A[2]
    
    ta = data.coords['t']
    timeref = ta[0]
    t = datetime_to_hour(ta, timeref)   #calcul l'intervalle en heure entre deux instant jours/h/m/s
    


    data = data*np.exp(-1j*omega*t)


    
    Wn = 2*dt*fc
    bb, aa = sig.butter(N,Wn,'low')
    iax_filt = data.dims.index(coord)
    filtwrap = lambda x: sig.filtfilt(bb, aa, x, axis=iax_filt, method="gust")
    ComplexAmplitude = xr.apply_ufunc(filtwrap, data.chunk({coord:-1}),dask="parallelized", output_dtypes=[data.dtype])
    #                      input_core_dims=[['time']], output_core_dims=[['time']],
    
    res = 2*(ComplexAmplitude*np.exp(1j*omega*t)).real

    return res, ComplexAmplitude


                                      
                                      
# Filtering
# to apply a good quality filter, we must wrap a scipy function (filtfilt) through xarray.apply_ufunc
def gauss_filt(ds_or_da, sigma=3, truncate=4, boundary="nearest"):
    """ apply scipy.ndimage.gaussian_filter to a xarray.DataArray, 
    along every dimension (assume isotropy and homogeneity of grid spacing)
    """
    _bnds = {"reflect":"reflect", "nearest":"nearest", "periodic":"wrap"}
        
    if isinstance(ds_or_da, xr.Dataset):
        res = xr.merge([gauss_filt(da, sigma=sigma, truncate=truncate, boundary=boundary) 
                         for da in ds_or_da.data_vars.values()])
    else:
        gf_kwgs = dict(sigma=sigma, truncate=truncate)
        if isinstance(boundary, (float, int)):
            gf_kwgs.update({"mode":"constant", "cval":boundary})
        else:
            gf_kwgs.update({"mode":_bnds[boundary]})
        res = ds_or_da.data.map_overlap(lambda x: gaussian_filter(x, **gf_kwgs), 
                                depth=sigma*truncate, boundary=boundary)
        res = xr.DataArray(res, dims=ds_or_da.dims).rename(ds_or_da.name)
    
    return res




# Filtering
# to apply a good quality filter, we must wrap a scipy function (filtfilt) through xarray.apply_ufunc
def myfilt(data, Wn, btype="low", coord="t"):
    """ wrapper to call scipy.signal.filtfilt using butterworth filter 
    using xarray apply_ufunc 
    Wn = 2*dt*fcut 
    Quick'n'dirty version 
    """
    bb, aa = sig.butter(4, Wn, btype=btype)
    iax_filt = data.dims.index(coord)
    filtwrap = lambda x: sig.filtfilt(bb, aa, x, axis=iax_filt, method="gust")
    if btype in ["high", "band"]: # remove coord-average if high-pass filtering
        res = xr.apply_ufunc(filtwrap, data.chunk({coord:-1}) - data.mean(coord), 
                             dask="parallelized", 
#                         input_core_dims=[['time']], output_core_dims=[['time']],
                             output_dtypes=[data.dtype])
    else:
        res = xr.apply_ufunc(filtwrap, data.chunk({coord:-1}), 
                             dask="parallelized", 
#                         input_core_dims=[['time']], output_core_dims=[['time']],
                             output_dtypes=[data.dtype])
    return res