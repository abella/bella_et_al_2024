import numpy as np
import xarray as xr
import dask

from xgcm import Grid

import scipy as sc
import scipy.signal as sig
from scipy.ndimage import gaussian_filter
from scipy.signal import freqz
from scipy.signal import butter, lfilter

from Routines import filtres

from rechunker import rechunk

import shutil

import numpy.ma as ma



def Domain_selection(data, domain_name, Dict_domain):
    # Apply a selection along a domain boundaries to a dataset or datarray
    
    # input:
    # data: dataset or datarray to apply the selection on.
    # domain_name: str
    # Dict_domain: dictionnary
    
    # output:
    # data: dataset or datarray reduced to the selection area.
    
    Domain_coords = Dict_domain[domain_name]
    Horizontal_dim = [d for d in data.dims if d[0] in ['x', 'y']]
    sel = {d:Domain_coords[d[0]] for d in Horizontal_dim}
    
    return data.sel(sel)








def Background_flow_horizontal_computing(du, dv, phi):
    # input
    # du : dataset of zonal velocity of the background flow: low pass filtered and corrected if the modal correction are needed (needed december 2022, may not be the case afterward). Contain modes 0 to wathever you want, on the T grid
    # dv : same thing as du but for the meridional velocity, on the T grid
    # Phi : dataset of pressure vertical mode containing modes 0 to wathever you want, on the T grid
    
    # output.
    # U : zonal velocity of the 3D plus t background flow, on the T grid
    # V : zonal velocity of the 3D plus t background flow, on the T grid
    
    

    mod_coord = du.coords['mode']
    U = du.isel(mode=0)* phi.isel(mode=0) * 0
    V = U
    for i in range(0,len(mod_coord),1):
        U = U + du.isel(mode=i) * phi.isel(mode=i)
        V = V + dv.isel(mode=i) * phi.isel(mode=i)
        
    return U, V








def build_temporal_series(datapath, filename, field_name, Domain, Dict_domain, datapath_plot, filename_plot):
    # Build continuous temporal series for the 8 months of the eNATL60 simulation available
    
    # input:
    # datapath : datapath were the data from which to build the temporal series is located
    # filename : name of the file of the data, must contains the number of the month
    # field)name : name of the variable
    # Domain : geographical domain over wich to compute the temporal series
    # Dict_domain : dictionary containning the domain names and their extension
    # datapath_plot : datapath in which to store the result
    # filename_plot : file in which to store the result
    for i in [7, 8, 9, 10, 11, 12, 1, 2]:
        print(i)
        data = xr.open_zarr(datapath/filename.format(i))
        data = utilities.Domain_selection(data[field_name], Domain, Dict_domain)
        del data.encoding['chunks']
        #data = data.chunk({"x_c":400, "y_c":400, "t":32, "mode":1, "modm":1})

        if i ==7:
            data.to_dataset(name=field_name).to_zarr(datapath_plot/filename_plot, compute=True, mode = 'w')
        else:
            data = data.isel(t=range(1, len(data.coords['t'].values), 1)) # Removal of the first value of the series that unfortunately start at
            # the last day of the previous months because of how the modal amplitude were filtered.
            data.to_dataset(name=field_name).to_zarr(datapath_plot/filename_plot, compute=True, mode = 'a', append_dim='t')

            
            








def Chunking(da, horizontal_chunking, vertical_chunking, temporal_chunking, modal_chunking):
    # da : array to chunck
    # horizontal_chunking : chunk size wanted for the horizontal dimensions.
    # vertical_chunking : chunk size wanted for the vertical dimensions.

    da = da.chunk({d:horizontal_chunking for d in da.dims if d[0] in "xy"})\
            .chunk({d:vertical_chunking for d in da.dims if d[0]=="z"}) \
            .chunk({d:temporal_chunking for d in da.dims if d[0]=="t"}) \
            .chunk({d:modal_chunking for d in da.dims if d[0]=="mod"})

    return da








def Complex_RMS(ds):
    #ds: complex data array you want to compute the rms of
    
    a = ds*ds.conj()
    a = a.real
    a = a.mean(dim="t", skipna="true")
    a = np.sqrt(a)
    a = a.persist()
    
    return a






def Complex_RMS_vectorXY(ds, dp):
    #ds: x component of complex data array you want to compute the rms of
    #dp: y component of complex data array you want to compute the rms of
    
    
    a = ds*ds.conj() + dp*dp.conj()
    a = a.real
    a = a.mean(dim="t", skipna="true")
    a = np.sqrt(a)
    a = a.persist()
    
    return a







def conj_stockage(data, variable_name, datapath, ch_h_store):
    for i in range(0,11,1):
        if i == 0:
            dataS = data.isel(mode=i).expand_dims(dim={'mode': 1})
            dataS.to_dataset(name=variable_name).to_zarr(datapath, mode='w', compute=True)
        else:
            dataS = data.isel(mode=i).expand_dims(dim={'mode': 1})
            dataS.to_dataset(name=variable_name).to_zarr(datapath, mode='a', compute=True, append_dim='mode')

            
            
            
            
            
            
def diff_interp_storage_interp(data, variable_name, datapath, chunking, chunking_store, dimx, dimy, axis, filename, xgrid):
    data.to_dataset(name=variable_name).to_zarr(datapath/"temp.zarr", compute=True, mode='w')
    data = xr.open_zarr(datapath/"temp.zarr")
    data = data[variable_name]
    data = data.chunk({dimx:chunking, dimy:chunking})
        
    data = xgrid.interp(data, 'X', boundary="extend")
    dim = [d for d in data.dims if d[0] == axis]
    dim = dim[0]
    data = data.chunk({dim : chunking})
        
    dimx = [d for d in data.dims if d[0] == 'x']
    dimx = dimx[0]
    dimy = [d for d in data.dims if d[0] == 'y']
    dimy = dimy[0]
    data = data.chunk({dimx:chunking_store, dimy:chunking_store})
    data.to_dataset(name=variable_name).to_zarr(datapath/"temp_1.zarr", compute=True, mode='w')
    shutil.rmtree(datapath/"temp.zarr")
                      
    data = xr.open_zarr(datapath/"temp_1.zarr")
    data = data[variable_name]
    data = data.chunk({dimx:chunking, dimy:chunking})
        
    data = xgrid.interp(data, 'Y', boundary="extend")
    dim = [d for d in data.dims if d[0] == axis]
    dim = dim[0]
    data = data.chunk({dim : chunking})
        
    dimx = [d for d in data.dims if d[0] == 'x']
    dimx = dimx[0]
    dimy = [d for d in data.dims if d[0] == 'y']
    dimy = dimy[0]
    data = data.chunk({dimx:chunking_store, dimy:chunking_store})
    data.to_dataset(name=variable_name).to_zarr(datapath/filename, compute=True, mode='w')
    shutil.rmtree(datapath/"temp_1.zarr")
    
    
def diff_interp_storage(data, axis, grid_start_to_end, chunking, chunking_store, datapath, filename, variable_name, horizontal_metric, xgrid):
    # compute the gradient of a quantities, then interpolate if needed and store it. The mretrics are taken into account
    
    # input :
    # data : datarray, data to differentiate.
    # axis : str, x or y lowercase, axis on which to compute the gradient.
    # grid_start_to_end : str, specify the starting grid (u,v,t,f,w) and the grid on wich the result is desired.
    # chunking : dictionnary, specify the chunking of the data for the computations.
    # chunking_store : dictionnary, specify the chunking of the data for storage.
    # datapath : path, place to store the result.
    # filename : filename of the result.
    # variable_name : str, name of the variable to be stored in the dataset.
    # horizontal_metric : datarray, metric used in the differentiation.
    # xgrid : xgcm grid object, need for the interpolation operations.
    
    
    if axis == 'x':
        Axis = 'X'
    if axis == 'y':
        Axis = 'Y'
    if axis == 'z':
        Axis = 'Z'
        

    #compute the differenciation, then get the horizontal dim allong wich it is computed, chunk it, then devide by the metric
    data = xgrid.diff(data, Axis, boundary='extend')
    dim = [d for d in data.dims if d[0] == axis]
    dim = dim[0]
    data = data.chunk({dim : chunking})
    data = data/horizontal_metric
        
    dimx = [d for d in data.dims if d[0] == 'x']
    dimx = dimx[0]
    dimy = [d for d in data.dims if d[0] == 'y']
    dimy = dimy[0]
    data = data.chunk({dimx:chunking_store, dimy:chunking_store})

    
    
    if grid_start_to_end == 'u_t':
        if axis == 'x':
            data.to_dataset(name=variable_name).to_zarr(datapath/filename, compute=True, mode='w')
    if grid_start_to_end == 'v_t':
        if axis == 'y':
            data.to_dataset(name=variable_name).to_zarr(datapath/filename, compute=True, mode='w')
            
    if grid_start_to_end == 'u_t':
        if axis == 'y':
            diff_interp_storage_interp(data, variable_name, datapath, chunking, chunking_store, dimx, dimy, axis, filename, xgrid)
            
    if grid_start_to_end == 'v_t':
        if axis == 'x':
            diff_interp_storage_interp(data, variable_name, datapath, chunking, chunking_store, dimx, dimy, axis, filename, xgrid)


     
    
    
    
    
    
    
    








def filtering_demod_warper(da, ds, omega,dt,fc,order_filtre):
    # Warper around the complex demodulation, compute the internal semiduirnal tides complexes modal amplitudes.


    # input 
    # da : field to filter in order to get the internal tides.
    # ds : general dataset, containing tmask, umask and vmask.
    # omega : pulsation used in the complex demodulation.
    # dt : temporal set of the tiem serie.
    # fc : Low pass cutting frequency to apply after the complex demod has been done.
    # order_filtre : Order of the low pass Butterworth filter.

    # Output
    # da : Compex amplitude of the field, filtered and subsampled to semi diurnal period.
    # Placeholder : Full filtered real signal.
    
    if "x_r" in da.dims:
        mask = ds.umask.isel(z_c=0)
    elif "y_r" in da.dims:
        mask = ds.vmask.isel(z_c=0)
    else:
        mask = ds.tmask.isel(z_c=0)

    da = da.where(mask, 0.)
     
    da = filtres.filtre_demodulation_complexe_passe_bas(da,omega,dt,fc,order_filtre,coord="t")
    
    #subsampling in time
    da = da.isel({d:slice(0, None,12) for d in ["t"]})
    # Avoiding complex 128 to get not too big data.
    #da = da.astype(dtype=np.complex64)
    
    
    
    return da #, Placeholder

    
    
    
    
    
    
    
def get_eNATL60_grid():
    # open eNATL60 grid and return a dataset merged containing all the coordinates as variables.
    
    grid_h0 = xr.open_zarr("/home/datawork-lops-osi/equinox/enatl60/grids/eNATL60_rest_grid.zarr")
    varnames = ['depth_c', 'depth_l', 'e1f', 'e1t', 'e1u', 'e1v', 'e2f', 'e2t', 'e2u', 'e2v', 'e3u', 'e3v', 'fmask', 'llat_cc', 'llat_cr', 'llat_rc', 'llat_rr', 'llon_cc', 'llon_cr', 'llon_rc', 'llon_rr', 'tmask', 'umask', 'vmask']
    grid_h = grid_h0.reset_coords(drop=True)
    
    for i in range(0, len(varnames),1):
        ds_h = grid_h0[varnames[i]]
        ds_h = ds_h.reset_coords(drop=True)
        grid_h = xr.merge([grid_h, ds_h])
    

    grid_z0 = xr.open_zarr("/home/datawork-lops-osi/equinox/enatl60/grids/eNATL60_global-mean_z-grid.zarr")
    varnames = ['depth_c', 'depth_l', 'llat_cc', 'llon_cc', 'tmaskutil', 'e3t', 'e3w']

    grid_z = grid_z0.reset_coords(drop=True)

    for i in range(0, len(varnames),1):
        ds_z = grid_z0[varnames[i]]
        ds_z = ds_z.reset_coords(drop=True)
        grid_z = xr.merge([grid_z, ds_z])
        
    grid = xr.merge([grid_z, grid_h], compat='override')
        
        
    return grid








def interp_chunking_storage(data, axis, chunking, chunking_store, coord, datapath, variable_name, xgrid):
    data = xgrid.interp(data, axis, boundary='extend').chunk({coord : chunking})
    data = data.chunk({'x_c':chunking_store, 'y_c':chunking_store})
    data.to_dataset(name=variable_name).to_zarr(datapath, compute=True, mode='w')

    
    
    
    
    
    
def masking(data, mask):
    data = data.assign_coords(mask=(["y_c", "x_c"], mask.data))
    data = data.where(data.mask)
    
    return data






def Mesoscale_filter_warper(da, ds, fc_meso, dt, fc,order_filtre):
    ### warper calling the filtering routine after some processing, to be used for the mesoscale which doesn't need complex demodulation, just low pass filter.
    
    ### input:
    # da : dataray, mesoscale field to filter.
    # ds : dataset containing umask, vmask and tmask mask for the U,Vand T 3D grids.
    # fc_meso : cutting frequency of the low pass fitler to be applied.
    # dt : time interval between to values of the temporal serie to filter.


    if "x_r" in da.dims:
        mask = ds.umask.isel(z_c=0)
    elif "y_r" in da.dims:
        mask = ds.vmask.isel(z_c=0)
    else:
        mask = ds.tmask.isel(z_c=0)

    da = da.where(mask, 0.)


    da = da.where(np.isfinite(da), 0.)
    dalp = filtres.myfilt(da, fc_meso*2*dt, btype="low", coord="t") #low pass filtering at 3 days period
    da = da.isel({d:slice(0, None,12) for d in ["t"]})

    return da





    
    
    
def Real_RMS(ds):
    #ds complex data array you want to compute the rms of
    
    a = ds*ds
    a = a.mean(dim="t", skipna="true")
    a = np.sqrt(a)
    
    return a






    

def rechunk_warper(data, target_chunking, max_mem, target_store, temp_store):
    data_chunking = rechunk(data,target_chunking , max_mem, target_store, temp_store=temp_store)
    data_chunking.execute()
    shutil.rmtree(temp_store)
    
    
    
    
    
    
    
    
def smoothing(data, sigma, xgrid):
    data2 = data.where(np.isfinite(data), 0.)
    for i in range(0,3,1):
        data1 = filtres.gauss_filt(data2, sigma=sigma)
        data2 = xr.where(np.isnan(data.values) == True, data1, data)      
    return data1








def storage_by_step(field, variable_name, filename, step, chunking_dict, datapath):
    # Compute and store a large datarray using sections on the y axis.
    
    # input : 
    # field : datarray, quantity to compute and store, supposed to be on the y_c coordinates.
    # variable_name : str, name you want to give to the dataset.
    # filename : str, name you want to give to the store.
    # step : integer, step along the y axis.
    # chunking_dict : dictionnary, chunking to use prior to storage.
    # datapath: Path, and not a string, otherwise it wont work. Where to stock the field.
    
    
    
    field = field.chunk(chunking_dict)
    if type(field) == xr.DataArray:
        field.to_dataset(name=variable_name).to_zarr(datapath/filename, compute=False, mode='w') #create the zarr store. 
    if type(field) == xr.Dataset:
        field.to_zarr(datapath/filename, compute=False, mode='w') #create the zarr store. 
    B = field.coords
    field = field.drop_vars(B)
    
    #detect if the y coordinate is y_r or y_c
    a = field.dims 
    for b in a:
        if b[0] == "y":
            y_coord = b
    
    
    for i in range(0, len(field.coords[y_coord]), step):
        LENGHT = len(field.coords[y_coord].values)
        print(i)
        a = min(step, LENGHT - i)
        A = slice(i,i+a)    
        fieldS = field.isel(y_c=A)
        fieldS = fieldS.chunk(chunking_dict)
        if type(field) == xr.DataArray:
            fieldS.to_dataset(name=variable_name).to_zarr(datapath/filename, compute=True, mode='r+', region = {'y_c': A}) #store in the zarr store created above region by region
        if type(field) == xr.Dataset:
            fieldS.to_zarr(datapath/filename, compute=True, mode='r+', region = {'y_c': A}) #store in the zarr store created above region by region

            
            
            
            
            
            

    
    
def Tab_energy_coupling_symetries(e1t, e2t, mask, data):
    x = data.coords['x_c']
    y = data.coords['y_c']
    
    e2t = e2t.assign_coords(x_c=x.values) # to ensure compatibility with spatial coordinates. Weird things happen otherwise.
    e2t = e2t.assign_coords(y_c=y.values)

    e1t = e1t.assign_coords(x_c=x.values)
    e1t = e1t.assign_coords(y_c=y.values)
    
    mask = mask.assign_coords(x_c=x.values)
    mask = mask.assign_coords(y_c=y.values)

    data = xr.where(data.mode < 4, data, data.sel(mode=range(4,11)).sum("mode")) #Groups higher modes as dissipation
    data = data.isel(mode=range(0,5,1))
    data = xr.where(data.modm < 4, data, data.sel(modm=range(4,11)).sum("modm")) #Groups higher modes as dissipation
    data = data.isel(modm=range(0,5,1))


    dataB = data * e1t * e2t
    dataB = dataB.assign_coords(mask=(["y_c", "x_c"], mask.data))
    dataB = dataB.where(dataB.mask)
    dataB = dataB.sum('x_c').sum('y_c')*1027

    dataC = dataB*0
    
    dataC = dataC.values
    dataB = dataB.values
    
    for i in range(0,5,1):
        for j in range(0,5,1):
            if i < j:
                dataC[i,j] = (dataB[i,j]-dataB[j,i]) * 0.5
            if i > j:
                dataC[i,j] = (dataB[i,j]+dataB[j,i]) * 0.5
            if i == j:
                dataC[i,j] = (dataB[i,j]+dataB[j,i]) * 0.5
                

                
    dataD = xr.DataArray(data=dataC, dims=["mode", "modm", "t"], coords=dict(t=(["t"], data.coords['t'].data)))
                
    return dataD

                    
                
                
                

    
    
    
    
    
    
    
    
    