import numpy as np
import xarray as xr
import dask

from xgcm import Grid

from Routines import utilities




def Flux_divergence(data_p, data_u, data_v, H, e1t, e2t, ch_h, xgrid):
    # Compute the Modal energy flux divergence

    # input:
    # data_p : pressure anomaly modal amplitude, T grid
    # data_u : zonal velocity modal amplitude, u grid
    # data_v : meridian velocity modal amplitude, v grid
    # H : topography, on T grid
    # e1t : x metric T grid
    # e2t : y metric T grid
    # ch_h : horizontal chunking of data
    # xgrid : xgcm grid object

    # output: 
    # DivF : Divergence of horizontal energy flux subsampled to diurnal period.



    data_p = xgrid.interp(data_p, "X", boundary="extend").chunk({"x_r":ch_h}) 
    H = xgrid.interp(H, 'X', boundary="extend").chunk({"x_r":ch_h})

    du = xgrid.diff((2 * data_u * data_p.conj() * H).real, 'X', boundary="extend").chunk({"x_c":ch_h})
    du = du/e1t  

    data_p = xgrid.interp(data_p, "X", boundary="extend").chunk({"x_c":ch_h}) #Le truc sait que la variable est sur la grille où elle est, et il l'interpole sur l'autre. Il faut stipuler l'axe et c'est tout.

    ## Interpolation de p et H au niveau de v
    data_p = xgrid.interp(data_p, "Y", boundary="extend").chunk({"y_r":ch_h}) #Le truc sait que la variable est sur la grille où elle est, et il l'interpole sur l'autre. Il faut stipuler l'axe et c'est tout.
    H = xgrid.interp(H, 'X', boundary="extend").chunk({"x_c":ch_h})
    H = xgrid.interp(H, 'Y', boundary="extend").chunk({"y_r":ch_h})

    dv = xgrid.diff((2 * data_v * data_p.conj() * H).real, 'Y', boundary="extend").chunk({"y_c":ch_h})
    dv = dv/e2t


    DivF = du + dv


    return DivF













def Cmn_computation(Tmn_x, Tmn_y, data_p, data_u, data_v, e1t, e2t, ch_h, ch_h_store, datapath_temp, xgrid):
    # Compute the modal coupling matrix caused by the topography and stationarry stratification gradients.

    # input: 
    # Tmn_x : x component of Tmn matrix, T grid
    # Tmn_y : y component of Tmn matrix, T grid
    # data_p : pressure anomaly modal amplitude, T grid
    # data_u : zonal velocity modal amplitude, u grid
    # data_v : meridian velocity modal amplitude, v grid
    # e1t : x axis metric, T grid
    # e2t : y axis metric, T grid
    # ch_h : horizontal chunking of data
    # datapath_temp: datapath where to store temporary results, except the result of a rechunk with rechunker.
    # xgrid : xgcm grid object

    # output: 
    # Cmn : Cmn topographic (and stationnary stratification) coupling term subsampled to diurnal period and subsampled on the horizontal.

    
    #interpolation at the center of the cell because of the Arakawa grid
    data_u = xgrid.interp(data_u, 'X', boundary="extend").chunk({"x_c":ch_h, "y_c":ch_h})
    data_v = xgrid.interp(data_v, 'Y', boundary="extend").chunk({"x_c":ch_h, "y_c":ch_h})
    
    data_u = data_u.chunk({"x_c":ch_h_store, "y_c":ch_h_store})
    data_u.to_dataset(name='datau').to_zarr(datapath_temp/"Data_u.zarr", compute=True, mode='w')
    data_u = xr.open_zarr(datapath_temp/"Data_u.zarr")
    data_u = data_u.datau.chunk({"x_c":ch_h, "y_c":ch_h})
    
    data_v = data_v.chunk({"x_c":ch_h_store, "y_c":ch_h_store})
    data_v.to_dataset(name='datav').to_zarr(datapath_temp/"Data_v.zarr", compute=True, mode='w')
    data_v = xr.open_zarr(datapath_temp/"Data_v.zarr")
    data_v = data_v.datav.chunk({"x_c":ch_h, "y_c":ch_h})
    


    
    
    
    ntom = {"mode":"modm"}
    remo = {"mode":"moden", "modm":"modem"}
    Tmn_x = Tmn_x.rename(remo)
    Tmn_y = Tmn_y.rename(remo)


    

        
    step = 400
            
            
    chunking_dict = {"x_c":ch_h_store, "y_c":ch_h_store, "t":16}
    
    HpmUnTnm_xF = (2*(data_p*data_u.rename(ntom).conj()).real) * Tmn_x.rename({"moden":"modm", "modem":"mode"})
    HpmUnTnm_xF = HpmUnTnm_xF.coarsen(x_c=2, boundary="trim").sum()
    HpmUnTnm_xF = HpmUnTnm_xF.coarsen(y_c=2, boundary="trim").sum()
    HpmUnTnm_xF = HpmUnTnm_xF.coarsen(t=2, boundary="trim").mean()
    utilities.storage_by_step(HpmUnTnm_xF, 'HpmUnTnm_x', "HpmUnTnm_x.zarr", step, chunking_dict, datapath_temp)  

    HpmUnTnm_yF = (2*(data_p*data_v.rename(ntom).conj()).real) * Tmn_y.rename({"moden":"modm", "modem":"mode"})
    HpmUnTnm_yF = HpmUnTnm_yF.coarsen(x_c=2, boundary="trim").sum()
    HpmUnTnm_yF = HpmUnTnm_yF.coarsen(y_c=2, boundary="trim").sum()
    HpmUnTnm_yF = HpmUnTnm_yF.coarsen(t=2, boundary="trim").mean()
    utilities.storage_by_step(HpmUnTnm_yF, 'HpmUnTnm_y', "HpmUnTnm_y.zarr", step, chunking_dict, datapath_temp)  
    
    HpmUnTnm_x = xr.open_zarr(datapath_temp/"HpmUnTnm_x.zarr")
    HpmUnTnm_x = HpmUnTnm_x.HpmUnTnm_x
    HpmUnTnm_x = HpmUnTnm_x.chunk({"mode":1})
    HpmUnTnm_y = xr.open_zarr(datapath_temp/"HpmUnTnm_y.zarr")
    HpmUnTnm_y = HpmUnTnm_y.HpmUnTnm_y
    HpmUnTnm_y = HpmUnTnm_y.chunk({"mode":1})
    
    HpmUnTnm = HpmUnTnm_x + HpmUnTnm_y
    
    

    HumPnTmn_xF = (2*(data_p.rename(ntom)*data_u.conj()).real) * Tmn_x.rename({"moden":"mode", "modem":"modm"})
    HumPnTmn_xF = HumPnTmn_xF.coarsen(x_c=2, boundary="trim").sum()
    HumPnTmn_xF = HumPnTmn_xF.coarsen(y_c=2, boundary="trim").sum()
    HumPnTmn_xF = HumPnTmn_xF.coarsen(t=2, boundary="trim").mean()
    utilities.storage_by_step(HumPnTmn_xF, 'HumPnTmn_x', "HumPnTmn_x.zarr", step, chunking_dict, datapath_temp)  
    
    HumPnTmn_yF = (2*(data_p.rename(ntom)*data_v.conj()).real) * Tmn_y.rename({"moden":"mode", "modem":"modm"})
    HumPnTmn_yF = HumPnTmn_yF.coarsen(x_c=2, boundary="trim").sum()
    HumPnTmn_yF = HumPnTmn_yF.coarsen(y_c=2, boundary="trim").sum()
    HumPnTmn_yF = HumPnTmn_yF.coarsen(t=2, boundary="trim").mean()
    utilities.storage_by_step(HumPnTmn_yF, 'HumPnTmn_y', "HumPnTmn_y.zarr", step, chunking_dict, datapath_temp)  
    
    HumPnTmn_x = xr.open_zarr(datapath_temp/"HumPnTmn_x.zarr")
    HumPnTmn_x = HumPnTmn_x.HumPnTmn_x
    HumPnTmn_x = HumPnTmn_x.chunk({"mode":1})
    HumPnTmn_y = xr.open_zarr(datapath_temp/"HumPnTmn_y.zarr")
    HumPnTmn_y = HumPnTmn_y.HumPnTmn_y
    HumPnTmn_y = HumPnTmn_y.chunk({"mode":1})
    
    HumPnTmn = HumPnTmn_x + HumPnTmn_y

    Cmn = HpmUnTnm-HumPnTmn
    Cmn = Cmn.reset_coords(drop=True)
    
    Cmn = Cmn.chunk({"x_c":ch_h_store, "y_c":ch_h_store})



    return Cmn








