Description of the procedure used to get the results for the paper Bella_et_al_2024.

Most of the codes are jupyter notebooks written in python. The rest are python scripts.

	
Libraries needed:
numpy,
xarray,
dask, 
xgcm,
pathlib,
shutil,
rechunker,
matplotlib,
scipy,
matplotlib,
cartopy.




Workflow:
1) Main_filtering_corrections.ipynb filter and isolate the mesoscale and internal tides contribution for the modal amplitude of the pressure anomaly, and zonal and meridional velocities. the mesoscale modal amplitude for the velocities, and the three internal tides modal amplitude are then corrected to account for the initial leakage of barotropic mode in the baroclinic modes, the initialy wrong stratification profile and the interpolation of the mode basis before projecting u and v.

2) Computation of vertical modes matrices and tensors such as Tmn, Umn ... with Modal_matrix.ipynb

3) Computation of the internal tide vertical velocity modal amplitude with Vertical_velocity.ipynb

4) Computation of the main contribution of the modal energy budget:
Energy flux divergence and topographic scattering: Main_DivF_Cmn_monthly_base.ipynb
Advection of uv : Main_Adevction_uv_monthly_base.ipynb
Advection of p : Main_Advection_p_monthly_base.ipynb
Horizontal shear of the background flow : Main_horizontal_shear_monthly_base.ipynb
Vertical shear of the background flow : Main_vertical_shear_monthly_base.ipynb
Buoyancy : Main_Buoyancy_Steady_monthly_base.ipynb

Not a main contribution:
The notebook for the computation of the variable stratification term.

5) Notebook with the codes used to plot the figures of the article: Plot_articles for non-map plot and preliminary processing for map plots, Plot_articles_cartopy for map plot. This notebook needs to be run on a machine with an internet acces for cartopy to load coastlines.




If you have questions, feel free to ask them at adrien.bella@inria.fr, valid adress until late 2024.


